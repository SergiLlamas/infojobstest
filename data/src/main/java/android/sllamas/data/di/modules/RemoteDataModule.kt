package android.sllamas.data.di.modules

import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.data.repositories.user.datasource.remote.UserRemoteDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
class RemoteDataModule {

  @Provides
  fun providesRetrofit(): Retrofit = Retrofit.Builder()
      .baseUrl("https://api.randomuser.me/")
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build()

  @Provides
  @Named("remote")
  fun providesUserRemoteDataSource(userRemoteDataSource: UserRemoteDataSource): UserDataSource = userRemoteDataSource
}