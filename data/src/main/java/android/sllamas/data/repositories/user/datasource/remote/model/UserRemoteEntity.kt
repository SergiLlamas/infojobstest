package android.sllamas.data.repositories.user.datasource.remote.model

import android.sllamas.domain.user.model.User
import com.google.gson.annotations.SerializedName

class UserRemoteEntity(
    @SerializedName("login") val login: LoginRemoteEntity?,
    @SerializedName("name") val name: NameRemoteEntity?,
    @SerializedName("email") val email: String?,
    @SerializedName("picture") val image: ImageRemoteEntity?,
    @SerializedName("phone") val phoneNumber: String?,
    @SerializedName("gender") val gender: String?,
    @SerializedName("location") val location: LocationRemoteEntity?,
    @SerializedName("registered") val registered: RegisteredRemoteEntity?
) {
    fun isValid() = login?.uuid != null
}

private fun UserRemoteEntity.transformToDomain() =
    User(
        uuid = login!!.uuid!!,
        name = (name?.name ?: DEFAULT_VALUE).capitalize(),
        surname = (name?.surname ?: DEFAULT_VALUE).capitalize(),
        email = email ?: DEFAULT_VALUE,
        thumbnailUrl = image?.thumbnailUrl ?: DEFAULT_VALUE,
        photoUrl = image?.largeImageUrl ?: DEFAULT_VALUE,
        phoneNumber = phoneNumber ?: DEFAULT_VALUE,
        gender = (gender ?: DEFAULT_VALUE).capitalize(),
        street = (location?.street ?: DEFAULT_VALUE).capitalize(),
        city = (location?.city ?: DEFAULT_VALUE).capitalize(),
        state = (location?.state ?: DEFAULT_VALUE).capitalize(),
        registeredDate = registered?.date
    )

fun List<UserRemoteEntity>.transformToDomain() =
    filter{ it.isValid() }.distinctBy{ it.login!!.uuid!! }.map { it.transformToDomain() }

private const val DEFAULT_VALUE = "-"