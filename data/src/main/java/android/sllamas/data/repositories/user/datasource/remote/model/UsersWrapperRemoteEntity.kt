package android.sllamas.data.repositories.user.datasource.remote.model

class UsersWrapperRemoteEntity(
    val results: List<UserRemoteEntity>?
)