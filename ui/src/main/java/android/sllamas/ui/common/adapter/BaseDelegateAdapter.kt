package android.sllamas.ui.common.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseDelegateAdapter : ViewTypeDelegateAdapter {

  override fun onCreateViewHolder(parent: ViewGroup) = attachHolder(parent)

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    holder as BindableViewHolder<ViewType>
    holder.bind(item)
  }
  abstract fun attachHolder(parent: ViewGroup): RecyclerView.ViewHolder
}