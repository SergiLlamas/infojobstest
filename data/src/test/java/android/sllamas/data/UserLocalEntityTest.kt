package android.sllamas.data

import android.sllamas.data.repositories.user.datasource.local.model.UserLocalEntity
import android.sllamas.data.repositories.user.datasource.local.model.transformToDomain
import android.sllamas.data.repositories.user.datasource.local.model.transformToLocalEntity
import android.sllamas.domain.user.model.User
import org.junit.Test
import org.junit.Assert.*

class UserLocalEntityTest {

  @Test
  fun transformToDomainIsCorrect() {
    val domainUser = listOf(userLocalEntity).transformToDomain()[0]

    assertEquals(userLocalEntity.uuid, domainUser.uuid)
    assertEquals(userLocalEntity.name, domainUser.name)
    assertEquals(userLocalEntity.surname, domainUser.surname)
    assertEquals(userLocalEntity.thumbnailUrl, domainUser.thumbnailUrl)
    assertEquals(userLocalEntity.photoUrl, domainUser.photoUrl)
    assertEquals(userLocalEntity.phoneNumber, domainUser.phoneNumber)
    assertEquals(userLocalEntity.gender, domainUser.gender)
    assertEquals(userLocalEntity.street, domainUser.street)
    assertEquals(userLocalEntity.city, domainUser.city)
    assertEquals(userLocalEntity.state, domainUser.state)
  }

  @Test
  fun transformToLocalEntityIsCorrect() {
    val localUserEntity = listOf(domainUser).transformToLocalEntity()[0]

    assertEquals(domainUser.uuid, localUserEntity.uuid)
    assertEquals(domainUser.name, localUserEntity.name)
    assertEquals(domainUser.surname, localUserEntity.surname)
    assertEquals(domainUser.thumbnailUrl, localUserEntity.thumbnailUrl)
    assertEquals(domainUser.photoUrl, localUserEntity.photoUrl)
    assertEquals(domainUser.phoneNumber, localUserEntity.phoneNumber)
    assertEquals(domainUser.gender, localUserEntity.gender)
    assertEquals(domainUser.street, localUserEntity.street)
    assertEquals(domainUser.city, localUserEntity.city)
    assertEquals(domainUser.state, localUserEntity.state)
  }

  private val userLocalEntity = UserLocalEntity(
      "uuid",
      "name",
      "surname",
      "email",
      "thumbnailUrl",
      "photoUrl",
      "phoneNumber",
      "gender",
      "street",
      "city",
      "state",
      "registeredDate"
  )

  private val domainUser = User(
      "uuid",
      "name",
      "surname",
      "email",
      "thumbnailUrl",
      "photoUrl",
      "phoneNumber",
      "gender",
      "street",
      "city",
      "state",
      "registeredDate"
  )
}