package android.sllamas.ui.list.adapter

import android.sllamas.ui.common.adapter.AdapterTypes
import android.sllamas.ui.common.adapter.BaseAdapterWithDelegates
import android.sllamas.ui.list.model.UserViewEntity

class UsersAdapter(onClickListener: (UserViewEntity) -> Unit) : BaseAdapterWithDelegates() {

  init {
    with(delegateAdapters) {
      put(AdapterTypes.USER.ordinal, UserDelegateAdapter(onClickListener))
    }
  }
}