package android.sllamas.ui.common.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseAdapterWithDelegates : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  var items: ArrayList<ViewType> = arrayListOf()
  val delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
      = delegateAdapters.get(viewType)!!.onCreateViewHolder(parent)

  override fun getItemViewType(position: Int) = items[position].getViewType()

  override fun getItemCount() = items.size

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    delegateAdapters.get(getItemViewType(position))!!.onBindViewHolder(holder, items[position])
  }

  fun addItems(items: List<ViewType>) {
    this.items.addAll(items)
    notifyDataSetChanged()
  }

  fun setItems(items: List<ViewType>) {
    this.items.clear()
    addItems(items)
  }
}