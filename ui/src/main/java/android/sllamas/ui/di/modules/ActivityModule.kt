package android.sllamas.ui.di.modules

import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {

  @Provides
  fun providesActivity(): AppCompatActivity {
    return activity
  }
}