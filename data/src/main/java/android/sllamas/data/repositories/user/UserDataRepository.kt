package android.sllamas.data.repositories.user

import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.domain.user.UserRepository
import android.sllamas.domain.user.model.User
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class UserDataRepository @Inject constructor(
    @Named("remote") private val remoteDataSource: UserDataSource,
    @Named("local") private val localDataSource: UserDataSource
): UserRepository {

  override fun getUsersList(count: Int): Single<List<User>> =
      remoteDataSource.getUsersList(count).doAfterSuccess { localDataSource.saveUsersList(it) }
}