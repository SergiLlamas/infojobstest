package android.sllamas.ui.list

import android.sllamas.domain.user.usecase.GetUsersList
import android.sllamas.ui.common.Presenter
import android.sllamas.ui.list.model.UserViewEntity
import android.sllamas.ui.list.model.transformToUi
import javax.inject.Inject

class UsersListPresenter @Inject constructor(
    private val getUsersList: GetUsersList
) : Presenter<UsersListPresenter.View>() {

  private val MAX_USERS_LIST = 40

  override fun onViewAttached() = with(getView()) {
    showLoading()
    hideView()
    addUsers(
        { users ->
          showUsersList(users.sortedBy { it.name })
          showView()
          hideLoading()
        },
        {
          println(it)
          hideLoading()
          showError(it)
        }
    )
  }

  fun addMoreUsers() = with(getView()) {
    showLoading()
    addUsers(
        {
          addUsersToList(it)
          hideLoading()
        },
        { showError(it) }
    )
  }

  private fun addUsers(onSuccess: (List<UserViewEntity>) -> Unit, onError: (String?) -> Unit) {
    val getUsersListDisposable = getUsersList.execute(MAX_USERS_LIST).subscribe(
        { onSuccess(it.transformToUi()) },
        { onError(it.message) }
    )
    addDisposable(getUsersListDisposable)
  }

  fun onUserSelected(user: UserViewEntity) {
    getView().showUserDetail(user)
  }

  interface View : Presenter.View {
    fun showUsersList(users: List<UserViewEntity>)
    fun showUserDetail(user: UserViewEntity)
    fun addUsersToList(users:List<UserViewEntity>)
    fun showView()
    fun hideView()
  }
}