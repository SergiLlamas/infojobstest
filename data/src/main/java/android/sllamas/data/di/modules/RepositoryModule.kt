package android.sllamas.data.di.modules

import android.sllamas.data.repositories.user.UserDataRepository
import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.domain.user.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class RepositoryModule {

  @Provides
  fun providesUserRepository(
      @Named("remote") userRemoteDataSource: UserDataSource,
      @Named("local") userLocalDataSource: UserDataSource
  ): UserRepository = UserDataRepository(userRemoteDataSource, userLocalDataSource)
}