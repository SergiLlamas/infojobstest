package android.sllamas.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.sllamas.ui.R
import android.sllamas.ui.common.BaseActivity
import android.sllamas.ui.common.extension.load
import android.sllamas.ui.di.components.ActivityComponent
import android.sllamas.ui.list.model.UserViewEntity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_user_detail.*

class UserDetailActivity : BaseActivity() {

  private val user by lazy { intent.getParcelableExtra(USER_EXTRA) as UserViewEntity }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_user_detail)
    initActionBar()
    initViews()
  }

  private fun initActionBar() = supportActionBar?.let {
    with(it) {
      title = "${user.name} ${user.surname}"
      setDisplayHomeAsUpEnabled(true)
      setDisplayShowHomeEnabled(true)
    }
  }

  private fun initViews() {
    photoImageView.load(user.photoUrl)
    genderTextView.text = user.gender
    streetTextView.text = user.street
    cityTextView.text = user.city
    stateTextView.text = user.state
    emailTextView.text = user.email
    registerDateTextView.text = user.registeredDate
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      android.R.id.home -> {
        finish()
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    private const val USER_EXTRA = "USER_EXTRA"

    fun startActivity(context: Context, user: UserViewEntity) {
      context.startActivity(
          Intent(context, UserDetailActivity::class.java).apply { putExtra(USER_EXTRA, user) }
      )
    }
  }

  override fun inject(component: ActivityComponent) = component.inject(this)
}