package android.sllamas.data.di.modules

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named

@Module
class SchedulersModule {

  @Provides
  @Named("main")
  fun providesMainThread(): Scheduler = AndroidSchedulers.mainThread()

  @Provides
  @Named("io")
  fun providesWorkerThread(): Scheduler = Schedulers.io()

}