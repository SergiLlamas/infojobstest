package android.sllamas.domain.user.usecase

import android.sllamas.domain.UseCase
import android.sllamas.domain.user.UserRepository
import android.sllamas.domain.user.model.User
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class GetUsersList @Inject constructor(
    private val repository: UserRepository,
    @Named("io") private val threadScheduler: Scheduler,
    @Named("main") private val postExecutionThread: Scheduler
) : UseCase<Int, List<User>>(threadScheduler, postExecutionThread) {

  override fun buildUseCaseSingle(params: Int): Single<List<User>> = repository.getUsersList(params)
}