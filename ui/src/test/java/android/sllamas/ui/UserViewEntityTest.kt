package android.sllamas.ui

import android.sllamas.domain.user.model.User
import android.sllamas.ui.list.model.transformToUi
import org.junit.Test
import org.junit.Assert.*

class UserViewEntityTest {

  @Test
  fun transformToUiIsCorrect() {
    val userViewEntity = listOf(domainUser).transformToUi()[0]

    assertEquals(domainUser.uuid, userViewEntity.uuid)
    assertEquals(domainUser.name, userViewEntity.name)
    assertEquals(domainUser.surname, userViewEntity.surname)
    assertEquals(domainUser.thumbnailUrl, userViewEntity.thumbnailUrl)
    assertEquals(domainUser.photoUrl, userViewEntity.photoUrl)
    assertEquals(domainUser.phoneNumber, userViewEntity.phoneNumber)
    assertEquals(domainUser.gender, userViewEntity.gender)
    assertEquals(domainUser.street, userViewEntity.street)
    assertEquals(domainUser.city, userViewEntity.city)
    assertEquals(domainUser.state, userViewEntity.state)
  }

  private val domainUser = User(
      "uuid",
      "name",
      "surname",
      "email",
      "thumbnailUrl",
      "photoUrl",
      "phoneNumber",
      "gender",
      "street",
      "city",
      "state",
      "registeredDate"
  )
}