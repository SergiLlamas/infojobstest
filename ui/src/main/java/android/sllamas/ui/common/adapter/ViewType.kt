package android.sllamas.ui.common.adapter

interface ViewType {
  fun getViewType(): Int
}