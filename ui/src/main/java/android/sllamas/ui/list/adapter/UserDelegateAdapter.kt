package android.sllamas.ui.list.adapter

import android.sllamas.ui.R
import android.sllamas.ui.common.adapter.BaseDelegateAdapter
import android.sllamas.ui.common.adapter.BindableViewHolder
import android.sllamas.ui.common.extension.inflate
import android.sllamas.ui.common.extension.load
import android.sllamas.ui.list.model.UserViewEntity
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_user.view.*

class UserDelegateAdapter(val listener: (UserViewEntity) -> Unit) : BaseDelegateAdapter() {

  override fun attachHolder(parent: ViewGroup) = UserViewHolder(parent.inflate(R.layout.item_user))

  inner class UserViewHolder(itemView: View) : BindableViewHolder<UserViewEntity>(itemView) {
    override fun bind(item: UserViewEntity) = with(itemView) {
      setOnClickListener { listener(item) }

      thumbnailImageView.load(item.thumbnailUrl)
      userNameTextView.text = item.name
      userSurnameTextView.text = item.surname
      phoneTextView.text = item.phoneNumber
      emailTextView.text = item.email
    }
  }
}