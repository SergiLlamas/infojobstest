package android.sllamas.data.repositories.user.datasource.remote

import android.sllamas.data.repositories.user.datasource.remote.model.UsersWrapperRemoteEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {

  @GET(".")
  fun getUsers(@Query("results") count: Int): Single<UsersWrapperRemoteEntity>
}