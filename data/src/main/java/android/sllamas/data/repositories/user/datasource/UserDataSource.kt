package android.sllamas.data.repositories.user.datasource

import android.sllamas.domain.user.model.User
import io.reactivex.Single

interface UserDataSource {

  fun getUsersList(count: Int): Single<List<User>>

  fun saveUsersList(users: List<User>): List<Long>
}