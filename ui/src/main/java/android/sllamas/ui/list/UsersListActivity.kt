package android.sllamas.ui.list

import android.os.Bundle
import android.sllamas.ui.R
import android.sllamas.ui.common.BaseActivity
import android.sllamas.ui.common.showToast
import android.sllamas.ui.detail.UserDetailActivity
import android.sllamas.ui.di.components.ActivityComponent
import android.sllamas.ui.list.adapter.UsersAdapter
import android.sllamas.ui.list.model.UserViewEntity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_users_list.*
import javax.inject.Inject

class UsersListActivity: BaseActivity(), UsersListPresenter.View {

  @Inject lateinit var presenter: UsersListPresenter
  private val usersAdapter by lazy { UsersAdapter(presenter::onUserSelected) }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_users_list)
    initializeCatalogsRecyclerView()
    presenter.attachView(this)
  }

  private fun initializeCatalogsRecyclerView() = with(usersListRecyclerView) {
    layoutManager = LinearLayoutManager(this@UsersListActivity)
    if (adapter == null) adapter = usersAdapter
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.user_list, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      R.id.add_more_users -> {
        presenter.addMoreUsers()
        return true
      }
    }
    return super.onOptionsItemSelected(item)
  }

  override fun onDestroy() {
    presenter.detachView()
    super.onDestroy()
  }

  override fun showUsersList(users: List<UserViewEntity>) {
    usersAdapter.setItems(users)
  }

  override fun addUsersToList(users: List<UserViewEntity>) {
    usersAdapter.addItems(users)
    with(usersAdapter.items) {
      distinctBy { (it as UserViewEntity).uuid }
      sortBy { (it as UserViewEntity).name }
    }
  }

  override fun showUserDetail(user: UserViewEntity) {
    UserDetailActivity.startActivity(this, user)
  }

  override fun showLoading() {
    progressBar.visibility = View.VISIBLE
  }

  override fun hideLoading() {
    progressBar.visibility = View.GONE
  }

  override fun showView() {
    usersListRecyclerView.visibility = View.VISIBLE
  }

  override fun hideView() {
    usersListRecyclerView.visibility = View.GONE
  }

  override fun showError(message: String?) {
    showToast(message)
  }

  override fun inject(component: ActivityComponent) = component.inject(this)
}