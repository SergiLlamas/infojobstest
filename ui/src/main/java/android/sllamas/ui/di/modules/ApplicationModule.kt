package android.sllamas.ui.di.modules

import android.content.Context
import android.sllamas.ui.InfojobsTestApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: InfojobsTestApplication) {

  @Provides
  @Singleton
  fun providesApplication(): InfojobsTestApplication = application

  @Provides
  @Singleton
  fun providesApplicationContext(): Context = application.applicationContext
}