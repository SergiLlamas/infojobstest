package android.sllamas.ui.list.model

import android.os.Parcel
import android.os.Parcelable
import android.sllamas.domain.user.model.User
import android.sllamas.ui.common.adapter.AdapterTypes
import android.sllamas.ui.common.adapter.ViewType

class UserViewEntity(
    val uuid: String,
    val name: String,
    val surname: String,
    val email: String,
    val thumbnailUrl: String,
    val photoUrl: String,
    val phoneNumber: String,
    val gender: String,
    val street: String,
    val city: String,
    val state: String,
    val registeredDate: String
) : ViewType, Parcelable {

  constructor(parcel: Parcel) : this(
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!,
      parcel.readString()!!
  )

  override fun getViewType() = AdapterTypes.USER.ordinal

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(uuid)
    parcel.writeString(name)
    parcel.writeString(surname)
    parcel.writeString(email)
    parcel.writeString(thumbnailUrl)
    parcel.writeString(photoUrl)
    parcel.writeString(phoneNumber)
    parcel.writeString(gender)
    parcel.writeString(street)
    parcel.writeString(city)
    parcel.writeString(state)
    parcel.writeString(registeredDate)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<UserViewEntity> {
    override fun createFromParcel(parcel: Parcel): UserViewEntity {
      return UserViewEntity(parcel)
    }

    override fun newArray(size: Int): Array<UserViewEntity?> {
      return arrayOfNulls(size)
    }
  }
}

private fun User.transformToUi() = UserViewEntity(
    uuid,
    name,
    surname,
    email,
    thumbnailUrl,
    photoUrl,
    phoneNumber,
    gender,
    street,
    city,
    state,
    getRegisteredDate()
)

fun List<User>.transformToUi() = map { it.transformToUi() }

private fun UserViewEntity.transformToDomain() = User(
    uuid,
    name,
    surname,
    email,
    thumbnailUrl,
    photoUrl,
    phoneNumber,
    gender,
    street,
    city,
    state,
    registeredDate
)

fun List<UserViewEntity>.transformToDomain() = map { it.transformToDomain() }