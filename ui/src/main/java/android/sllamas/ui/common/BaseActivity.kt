package android.sllamas.ui.common

import android.os.Bundle
import android.sllamas.ui.InfojobsTestApplication
import android.sllamas.ui.di.components.ActivityComponent
import android.sllamas.ui.di.modules.ActivityModule
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    initializeDependencyInjection()
  }

  private fun initializeDependencyInjection() {
    val component = (application as InfojobsTestApplication).component.plus(ActivityModule(this))

    inject(component)
  }

  abstract fun inject(component: ActivityComponent)
}