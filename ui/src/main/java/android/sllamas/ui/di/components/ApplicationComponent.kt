package android.sllamas.ui.di.components

import android.sllamas.data.di.modules.LocalDataModule
import android.sllamas.data.di.modules.RemoteDataModule
import android.sllamas.data.di.modules.RepositoryModule
import android.sllamas.data.di.modules.SchedulersModule
import android.sllamas.ui.InfojobsTestApplication
import android.sllamas.ui.di.modules.ActivityModule
import android.sllamas.ui.di.modules.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (RepositoryModule::class), (SchedulersModule::class), (RemoteDataModule::class), (LocalDataModule::class)])
interface ApplicationComponent {

  fun inject(application: InfojobsTestApplication)

  fun application() : InfojobsTestApplication

  fun plus(activityModule: ActivityModule) : ActivityComponent
}