package android.sllamas.ui.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<V : Presenter.View> {

  private var view: V? = null
  private var disposables = CompositeDisposable()

  fun attachView(view: V) {
    this.view = view
    onViewAttached()
  }

  protected abstract fun onViewAttached()

  open fun detachView() {
    this.view = null
    disposables.dispose()
  }

  fun dispose() {
    if (!disposables.isDisposed) {
      disposables.dispose()
    }
  }

  fun clearDisposable() {
    disposables.clear()
  }

  fun addDisposable(disposable: Disposable) {
    if (disposables.isDisposed) {
      disposables = CompositeDisposable()
    }
    disposables.add(disposable)
  }

  fun getView(): V = view!!

  interface View {
    fun showLoading()
    fun hideLoading()
    fun showError(message: String?)
  }
}