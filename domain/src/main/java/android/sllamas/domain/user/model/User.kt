package android.sllamas.domain.user.model

import android.sllamas.domain.commons.getApiDateFormatted

class User(
    val uuid: String,
    val name: String,
    val surname: String,
    val email: String,
    val thumbnailUrl: String,
    val photoUrl: String,
    val phoneNumber: String,
    val gender: String,
    val street: String,
    val city: String,
    val state: String,
    private val registeredDate: String?
) {

  fun getRegisteredDate() = getApiDateFormatted(registeredDate)
}