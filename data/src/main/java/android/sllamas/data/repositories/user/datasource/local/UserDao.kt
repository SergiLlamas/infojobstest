package android.sllamas.data.repositories.user.datasource.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.sllamas.data.repositories.user.datasource.local.model.UserLocalEntity
import io.reactivex.Single

@Dao
interface UserDao {

  @Query("select * from user limit :count")
  fun getUsersList(count: Int): Single<List<UserLocalEntity>>

  @Insert(onConflict = REPLACE)
  fun saveUsersList(users: List<UserLocalEntity>): List<Long>
}