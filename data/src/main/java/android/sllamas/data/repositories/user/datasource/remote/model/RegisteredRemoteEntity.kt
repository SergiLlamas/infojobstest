package android.sllamas.data.repositories.user.datasource.remote.model

import com.google.gson.annotations.SerializedName

class RegisteredRemoteEntity(
    @SerializedName("date") val date: String?
)