package android.sllamas.ui.common.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BindableViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

  abstract fun bind(item: T): Unit?
}