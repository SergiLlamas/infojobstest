package android.sllamas.data.repositories.user.datasource.remote.model

import com.google.gson.annotations.SerializedName

class LocationRemoteEntity(
    @SerializedName("street") val street: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state") val state: String?
)