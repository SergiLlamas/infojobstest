package android.sllamas.data.repositories.user.datasource.remote

import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.data.repositories.user.datasource.remote.model.transformToDomain
import android.sllamas.domain.user.model.User
import io.reactivex.Single
import retrofit2.Retrofit
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val retrofit: Retrofit) : UserDataSource {

  override fun getUsersList(count: Int): Single<List<User>> =
      retrofit.create(UserApi::class.java).getUsers(count).map { it.results?.transformToDomain() }

  override fun saveUsersList(users: List<User>): List<Long> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}