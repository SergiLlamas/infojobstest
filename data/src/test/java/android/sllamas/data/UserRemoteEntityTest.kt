package android.sllamas.data

import android.sllamas.data.repositories.user.datasource.remote.model.*
import org.junit.Assert.assertEquals
import org.junit.Test

class UserRemoteEntityTest {

  @Test
  fun transformToDomainIsCorrect() {
    val domainUser = listOf(userRemoteEntity).transformToDomain()[0]

    assertEquals(userRemoteEntity.login!!.uuid, domainUser.uuid)
    assertEquals(userRemoteEntity.name!!.name!!.capitalize(), domainUser.name)
    assertEquals(userRemoteEntity.name!!.surname!!.capitalize(), domainUser.surname)
    assertEquals(userRemoteEntity.image!!.thumbnailUrl, domainUser.thumbnailUrl)
    assertEquals(userRemoteEntity.image!!.largeImageUrl, domainUser.photoUrl)
    assertEquals(userRemoteEntity.phoneNumber, domainUser.phoneNumber)
    assertEquals(userRemoteEntity.gender!!.capitalize(), domainUser.gender)
    assertEquals(userRemoteEntity.location!!.street!!.capitalize(), domainUser.street)
    assertEquals(userRemoteEntity.location!!.city!!.capitalize(), domainUser.city)
    assertEquals(userRemoteEntity.location!!.state!!.capitalize(), domainUser.state)
  }

  private val userRemoteEntity = UserRemoteEntity(
      LoginRemoteEntity("uuid"),
      NameRemoteEntity("treatment", "name", "surname"),
      "email",
      ImageRemoteEntity("largeImageUrl", "thumbnailUrl"),
      "phoneNumber",
      "gender",
      LocationRemoteEntity("street", "city", "state"),
      RegisteredRemoteEntity("date")
  )
}