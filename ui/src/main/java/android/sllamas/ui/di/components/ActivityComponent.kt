package android.sllamas.ui.di.components

import android.sllamas.ui.detail.UserDetailActivity
import android.sllamas.ui.di.modules.ActivityModule
import android.sllamas.ui.di.scopes.PerActivity
import android.sllamas.ui.list.UsersListActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [(ActivityModule::class)])
interface ActivityComponent {

  fun inject(usersListActivity: UsersListActivity)

  fun inject(userDetailActivity: UserDetailActivity)
}