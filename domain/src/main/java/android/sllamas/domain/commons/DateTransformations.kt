package android.sllamas.domain.commons

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun getApiDateFormatted(apiDateString: String?): String {
  val locale = Locale.getDefault() //TODO keep in mind translations and device language
  val apiDateFormat = SimpleDateFormat(API_DATE_FORMAT, locale)
  val userDateFormat = SimpleDateFormat(USER_DATE_FORMAT, locale)

  return try {
    val apiDate = apiDateFormat.parse(apiDateString)
    userDateFormat.format(apiDate).capitalize()
  } catch (e: ParseException) {
    DEFAULT_STRING
  }
}

private const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
private const val USER_DATE_FORMAT = "MMM dd, yyyy"
private const val DEFAULT_STRING = "-"