package android.sllamas.data.repositories.user.datasource.remote.model

import com.google.gson.annotations.SerializedName

class ImageRemoteEntity(
    @SerializedName("large") val largeImageUrl: String?,
    @SerializedName("thumbnail") val thumbnailUrl: String?
)