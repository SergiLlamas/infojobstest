package android.sllamas.data.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import android.sllamas.data.common.DATABASE_NAME
import android.sllamas.data.common.UserDatabase
import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.data.repositories.user.datasource.local.UserDao
import android.sllamas.data.repositories.user.datasource.local.UserLocalDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class LocalDataModule {

  private var databaseInstance: UserDatabase? = null

  @Singleton
  @Provides
  fun providesRoomDatabase(context: Context): UserDatabase {
    if (databaseInstance == null) {
      databaseInstance = Room.databaseBuilder(context, UserDatabase::class.java, DATABASE_NAME).build()
    }
    return databaseInstance!!
  }

  @Singleton
  @Provides
  fun providesProductDao(): UserDao = databaseInstance!!.getUserDao()

  @Provides
  @Named("local")
  fun providesUserLocalDataSource(userLocalDataSource: UserLocalDataSource): UserDataSource = userLocalDataSource
}