package android.sllamas.data.common

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.sllamas.data.repositories.user.datasource.local.UserDao
import android.sllamas.data.repositories.user.datasource.local.model.UserLocalEntity

@Database(entities = [(UserLocalEntity::class)], version = DATABASE_VERSION, exportSchema = false)
abstract class UserDatabase: RoomDatabase() {

  abstract fun getUserDao(): UserDao
}

const val DATABASE_VERSION = 1
const val DATABASE_NAME = "random-users-database"