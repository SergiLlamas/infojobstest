package android.sllamas.data.repositories.user.datasource.remote.model

import com.google.gson.annotations.SerializedName

class LoginRemoteEntity(
    @SerializedName("uuid") val uuid: String?
)