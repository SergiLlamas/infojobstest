package android.sllamas.data.repositories.user.datasource.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.sllamas.domain.user.model.User

@Entity(tableName = "user")
class UserLocalEntity(
    @PrimaryKey var uuid: String,
    var name: String,
    var surname: String,
    var email: String,
    var thumbnailUrl: String,
    var photoUrl: String,
    var phoneNumber: String,
    var gender: String,
    var street: String,
    var city: String,
    var state: String,
    var registeredDate: String
)

private fun UserLocalEntity.transformToDomain() =
    User(
        uuid,
        name,
        surname,
        email,
        thumbnailUrl,
        photoUrl,
        phoneNumber,
        gender,
        street,
        city,
        state,
        registeredDate
    )

fun List<UserLocalEntity>.transformToDomain() = map { it.transformToDomain() }

private fun User.transformToLocalEntity() =
    UserLocalEntity(
        uuid,
        name,
        surname,
        email,
        thumbnailUrl,
        photoUrl,
        phoneNumber,
        gender,
        street,
        city,
        state,
        getRegisteredDate()
    )

fun List<User>.transformToLocalEntity() = map { it.transformToLocalEntity() }