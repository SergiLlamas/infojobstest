package android.sllamas.domain.user

import android.sllamas.domain.user.model.User
import io.reactivex.Single

interface UserRepository {

  fun getUsersList(count: Int): Single<List<User>>
}