package android.sllamas.ui

import android.app.Application
import android.sllamas.ui.di.components.ApplicationComponent
import android.sllamas.ui.di.components.DaggerApplicationComponent
import android.sllamas.ui.di.modules.ApplicationModule

class InfojobsTestApplication : Application() {

  val component: ApplicationComponent
    get() = DaggerApplicationComponent.builder()
        .applicationModule(ApplicationModule(this))
        .build()
}