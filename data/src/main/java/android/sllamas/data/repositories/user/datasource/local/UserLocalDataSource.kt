package android.sllamas.data.repositories.user.datasource.local

import android.sllamas.data.common.UserDatabase
import android.sllamas.data.repositories.user.datasource.UserDataSource
import android.sllamas.data.repositories.user.datasource.local.model.transformToDomain
import android.sllamas.data.repositories.user.datasource.local.model.transformToLocalEntity
import android.sllamas.domain.user.model.User
import io.reactivex.Single
import javax.inject.Inject

class UserLocalDataSource @Inject constructor(private val database: UserDatabase) : UserDataSource {

  override fun getUsersList(count: Int): Single<List<User>> =
      database.getUserDao().getUsersList(count).map { it.transformToDomain() }

  override fun saveUsersList(users: List<User>): List<Long> =
      database.getUserDao().saveUsersList(users.transformToLocalEntity())
}