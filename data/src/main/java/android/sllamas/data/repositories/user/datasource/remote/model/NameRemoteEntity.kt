package android.sllamas.data.repositories.user.datasource.remote.model

import com.google.gson.annotations.SerializedName

class NameRemoteEntity(
    @SerializedName("title") val treatment: String?,
    @SerializedName("first") val name: String?,
    @SerializedName("last") val surname: String?
)
