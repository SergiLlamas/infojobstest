package android.sllamas.domain

import android.sllamas.domain.commons.getApiDateFormatted
import org.junit.Test
import org.junit.Assert.*

class DateTransformationsTest {

  @Test
  fun dateTransformationIsCorrect() {
    val apiDate = "1977-02-02T04:29:33Z"

    assertEquals("Feb 02, 1977", getApiDateFormatted(apiDate))
  }
}